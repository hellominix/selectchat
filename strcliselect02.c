#include	"io.h"

void
str_cli(FILE *fp, int sockfd,char*argv)
{
	int			maxfdp1, stdineof;
	fd_set		rset;
	char		buf[MAXLINE];
        char		buf_w[MAXLINE];
	int		n;
        int             i;

	stdineof = 0;
	FD_ZERO(&rset);
	for ( ; ; ) {
		if (stdineof == 0)
			FD_SET(fileno(fp), &rset);
		FD_SET(sockfd, &rset);
		maxfdp1 = max(fileno(fp), sockfd) + 1;
		select(maxfdp1, &rset, NULL, NULL, NULL);

		if (FD_ISSET(sockfd, &rset)) {	/* socket is readable */
			if ( (n = read(sockfd, buf, MAXLINE)) == 0) {
				if (stdineof == 1)
					return;		/* normal termination */
				else
					{
           perror("str_cli: server terminated prematurely");
           exit(1); 
          }
			}
                      
			write(fileno(stdout), buf, n);
		}

		if (FD_ISSET(fileno(fp), &rset)) {
           
          
                              /* input is readable */
  if ( (n = read(fileno(fp), buf, MAXLINE)) == 0)
     {
				stdineof = 1;
				shutdown(sockfd, SHUT_WR);	/* send FIN */
				FD_CLR(fileno(fp), &rset);
				continue;
			}                 
                        //printf("\nthere are %d characters in buf\n",n);
                        buf[n]='\0';
                        //printf("%s:\n",buf);  
                        strcpy(buf_w,argv);
                       
                        strcat(buf_w,":");
                        
                        strncat(buf_w,buf,strlen(buf));
												buf_w[strlen(buf_w)] = '\0';
                       // printf("%s",buf_w);
			                  write(sockfd, buf_w,strlen(buf_w));
		}
	}
}
